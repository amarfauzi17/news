<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class VisitorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	$dates = $this->dateRange(date("Y-m-d H:i:s", strtotime("-7 days")), date("Y-m-d H:i:s"));
    	for ($i=1; $i <=120 ; $i++) {
    		if (empty($ipv4)) {
    			$ipv4 = $faker->ipv4();
    		}
    		$post   = Post::where('type', 1)->inRandomOrder()->first();
    		$date   = $dates[array_rand($dates)];
    		$data[] = [
    			'method'  	 => 'GET',
    			'request' 	 => json_encode([]),
    			'url'     	 => route('post.show', $post->slug),
    			'languages'  => json_encode(["en-us","en"]),
    			'useragent'  => $faker->userAgent(),
    			'ip'         => $ipv4,
    			'created_at' => $date,
    			'updated_at' => $date,
    		];
    		if ($i % 3 == 0) {
    			unset($ipv4);
    		}
    	}

    	DB::table('shetabit_visits')->insert($data);
    }

    private function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d H:i:s' ) {

    	$dates = array();
    	$current = strtotime( $first );
    	$last = strtotime( $last );

    	while( $current <= $last ) {

    		$dates[] = date( $format, $current );
    		$current = strtotime( $step, $current );
    	}

    	return $dates;
    }
}
