<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'view categories']);
        Permission::create(['name' => 'create categories']);
        Permission::create(['name' => 'edit categories']);
        Permission::create(['name' => 'delete categories']);

        Permission::create(['name' => 'view comments']);
        Permission::create(['name' => 'reply comments']);
        Permission::create(['name' => 'delete comments']);

        Permission::create(['name' => 'view messages']);
        Permission::create(['name' => 'delete messages']);

        Permission::create(['name' => 'view newsletters']);
        Permission::create(['name' => 'delete newsletters']);

    	Permission::create(['name' => 'view pages']);
    	Permission::create(['name' => 'create pages']);
    	Permission::create(['name' => 'edit pages']);
    	Permission::create(['name' => 'delete pages']);

        Permission::create(['name' => 'view posts']);
        Permission::create(['name' => 'create posts']);
        Permission::create(['name' => 'edit posts']);
        Permission::create(['name' => 'delete posts']);
    	Permission::create(['name' => 'publish posts']);
    	Permission::create(['name' => 'unpublish posts']);

        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'edit roles']);
        Permission::create(['name' => 'delete roles']);

        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);
        Permission::create(['name' => 'reset password users']);

    	$writerRole = Role::create(['name' => 'writer']);
    	$writerRole->givePermissionTo('view posts');
    	$writerRole->givePermissionTo('create posts');
    	$writerRole->givePermissionTo('edit posts');
    	$writerRole->givePermissionTo('delete posts');

    	$adminRole = Role::create(['name' => 'author']);
    	$adminRole->givePermissionTo('view posts');
    	$adminRole->givePermissionTo('create posts');
    	$adminRole->givePermissionTo('edit posts');
    	$adminRole->givePermissionTo('delete posts');
    	$adminRole->givePermissionTo('publish posts');
    	$adminRole->givePermissionTo('unpublish posts');
        $adminRole->givePermissionTo('view categories');
        $adminRole->givePermissionTo('create categories');
        $adminRole->givePermissionTo('edit categories');
        $adminRole->givePermissionTo('delete categories');

    	User::all()->each(function($user) {
    		$roles = ['writer', 'author'];
            $user->assignRole($roles[array_rand($roles)]);
        });

    	$superadminRole = Role::create(['name' => 'admin']);

    	$user = User::factory()->create([
    		'name'     => 'admin',
    		'email'    => 'admin@mail.com',
    		'password' => bcrypt('secret')
    	]);
    	$user->assignRole($superadminRole);
    }
}
