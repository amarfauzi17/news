<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(5)->create();
        \App\Models\Category::factory(10)->create();
        \App\Models\Post::factory(30)->create()->each(function($post) {
            $post->categories()->sync(\App\Models\Category::all()->random(3));
        });
        \App\Models\ContactUs::factory(30)->create();
        \App\Models\Newsletter::factory(15)->create();
        \App\Models\Comment::factory(15)->create();
        $this->call(PageSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(VisitorSeeder::class);
    }
}
