<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->text('comment');
            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('reply_to')->nullable();
            $table->timestamps();

            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('CASCADE')->onDelete('CASCADE');
            // $table->foreign('parent_id')->references('id')->on('comments')->onUpdate('CASCADE')->onDelete('CASCADE');
            // $table->foreign('reply_to')->references('id')->on('comments')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->after('reply_to')->nullable();
            $table->unsignedBigInteger('updated_by')->after('created_at')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign(['post_id']);
            // $table->dropForeign(['parent_id']);
            // $table->dropForeign(['reply_to']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });
        Schema::dropIfExists('comments');
    }
};
