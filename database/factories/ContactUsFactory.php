<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\ContactUs;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ContactUsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = ContactUs::class;

    public function definition()
    {
        return [
            'name'    => $this->faker->name(),
            'email'   => $this->faker->unique()->safeEmail(),
            'website' => parse_url($this->faker->url())['host'] ?? null,
            'phone'   => preg_replace("/[^0-9]/", "", $this->faker->phoneNumber()),
            'subject' => $this->faker->sentence(1),
            'message' => $this->faker->paragraph(6),
        ];
    }
}
