<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Post::class;

    public function definition()
    {
        $author  = User::inRandomOrder()->first();
        $title   = $this->faker->unique()->sentence(10);
        $rand    = rand(3, 6);
        $image   = "tech_blog_0".rand(1, 9).".jpg";
        $content = '<img class="img-fluid mx-auto d-block" src="/images/posts/'.$image.'" alt="'.$this->faker->sentence(3).'" title="'.$title.'"/><p></p>';
        for ($i=1; $i <= $rand; $i++) { 
            $content .= "<p>".$this->faker->sentence(50)."</p>";
        }
        return [
            'title'   => $title,
            'slug'    => Str::slug($title),
            'thumbnail' => $image,
            'content' => $content,
            'meta_title' => $title,
            'meta_description' => $title,
            'published_at' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'status'  => 2,
            'type'    => 1,
            'created_by' => $author->id,
            'updated_by' => $author->id,
        ];
    }
}
