<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BlameableObserver
{
	public function creating(Model $model)
	{
		if (!isset($model->created_by)) {
			$model->created_by = Auth::user()->id ?? null;
		}
		if (!isset($model->updated_by)) {
			$model->updated_by = Auth::user()->id ?? null;
		}
	}

	public function updating(Model $model)
	{
		if (!isset($model->updated_by)) {
			$model->updated_by = Auth::user()->id ?? null;
		}
	}
}