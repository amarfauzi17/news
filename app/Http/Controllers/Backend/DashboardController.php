<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
	public function index()
	{
		$visits = DB::table('shetabit_visits')->where('method', 'GET')->whereBetween('created_at', [date("Y-m-d H:i:s", strtotime("-7 days")), date("Y-m-d H:i:s")])->get()->groupBy(function($item) {
			return date('Y-m-d', strtotime($item->created_at));
		});
		$count_posts = Post::where('type', 1)->count();
		$count_views = DB::table('shetabit_visits')->where('method', 'GET');
		$top_posts   = DB::select("SELECT p.title, COUNT(slug) as total FROM posts p INNER JOIN shetabit_visits v ON v.url LIKE CONCAT('%/', p.slug) GROUP BY slug ORDER BY total DESC limit 5;");
		return view('backend.dashboard', compact('visits', 'count_posts', 'count_views', 'top_posts'));
	}
}
