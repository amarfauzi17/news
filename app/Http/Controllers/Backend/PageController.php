<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:view pages', ['only' => ['index']]);
        $this->middleware('permission:create pages', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit pages', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete pages', ['only' => ['destroy']]);
    }

    public function index()
    {
        $pages = Post::where('type', 2)->orderByDesc('created_at')->get();
        return view('backend.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:posts,title',
            'slug'  => [
                'required',
                Rule::unique('posts')->where(function($query) {
                    return $query->where('type', 2);
                })
            ],
            'content' => 'required',
        ]);

        $payloads = $request->except('_token');
        $payloads = removeHtmlTagsOfFields($payloads, ['content']);
        $html     = str_get_html($request->content);
        $payloads['type'] = 2;
        $payloads['status'] = 2;
        $payloads['published_at'] = date('Y-m-d H:i:s');
        $page     = Post::create($payloads);
        if ($page) {
            return redirect()->route('dashboard.page.index')->with('success', 'page '.$page->title.' saved successfully');
        }
        return redirect()->back()->with('error', 'failed, Try Again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Post::where(['id' => $id, 'type' => 2])->first();
        if($page){
            return view('backend.pages.edit', compact('page'));
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Post::where(['id' => $id, 'type' => 2])->first();
        if ($page) {
            $request->validate([
                'title' => [
                    'required',
                    Rule::unique('posts')->where(function($query) use ($page){
                        return $query->where([['id', '<>', $page->id], ['type', '=', 2]]);
                    })
                ],
                'slug'  => [
                    'required',
                    Rule::unique('posts')->where(function($query) use ($page){
                        return $query->where([['id', '<>', $page->id], ['type', '=', 2]]);
                    })
                ],
                'content' => 'required',
            ]);

            $payloads = $request->except('_token', '_method');
            $payloads = removeHtmlTagsOfFields($payloads, ['content']);
            $upd      = $page->update($payloads);
            if ($upd) {
                return redirect()->route('dashboard.page.index')->with('success', 'page '.$page->title.' updated successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Post::where(['id' => $id, 'type' => 2])->first();
        if ($page) {
            $delete = $page->delete();
            if ($delete) {
                return redirect()->back()->with('success', 'page '.$page->title.' deleted successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }
}
