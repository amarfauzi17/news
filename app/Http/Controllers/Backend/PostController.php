<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:view posts', ['only' => ['index']]);
        $this->middleware('permission:create posts', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit posts', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete posts', ['only' => ['destroy']]);
    }

    public function index()
    {
        $this->postScheduleUpdate();
        $posts  = Post::with('categories')->where('type', 1)->orderByDesc('created_at')->get();
        $status = [1 => 'Draft', 2 => 'Published', 3 => 'Schedule', 4 => 'Unpublish'];
        return view('backend.posts.index', compact('posts', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name')->get();
        return view('backend.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status    = auth()->user()->can('publish posts') ? [1,2,3] : [1];
        $published = $request->status == 3 ? 'required|after:'.date('Y-m-d').' 00:00:00' : 'nullable';
        $request->validate([
            'title' => [
                'required',
                Rule::unique('posts')->where(function($query){
                    return $query->where('type', 1);
                })
            ],
            'slug'  => [
                'required',
                Rule::unique('posts')->where(function($query){
                    return $query->where('type', 1);
                })
            ],
            'categories'   => 'required|exists:categories,id',
            'content'      => 'required',
            'status'       => 'required|in:'.implode(', ', $status),
            'published_at' => $published
        ]);

        $payloads = $request->except('_token', 'categories');
        $payloads = removeHtmlTagsOfFields($payloads, ['content']);
        $html  = str_get_html($request->content);
        $image = null;
        if ($html) {
            $image = $html->find('img', 0);
            $image = empty($image) ? null : basename($image->src);
        }
        $payloads['thumbnail'] = $image;
        $payloads['type'] = 1;
        $post = Post::create($payloads);
        if ($post) {
            $post->categories()->sync($request->categories);
            return redirect()->route('dashboard.post.index')->with('success', 'post '.$post->title.' saved successfully');
        }
        return redirect()->back()->with('error', 'failed, Try Again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::orderBy('name')->get();
        $post = Post::with('categories')->where(['id' => $id, 'type' => 1])->first();
        if($post){
            return view('backend.posts.edit', compact('categories', 'post'));
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where(['id' => $id, 'type' => 1])->first();
        if ($post) {
            $permission = auth()->user()->can('publish posts');
            if ($permission) {
                $status    = 'required|in:1,2,3';
                $published = $request->status == 3 ? 'required|after:'.date('Y-m-d').' 00:00:00' : 'nullable';
            }else{
                $status    = 'nullable';
                $published = 'nullable';
            }
            $request->validate([
                'title'        => [
                    'required',
                    Rule::unique('posts')->where(function($query) use ($post){
                        return $query->where([['id', '<>', $post->id], ['type', '=', 1]]);
                    })
                ],
                'slug'         => [
                    'required',
                    Rule::unique('posts')->where(function($query) use ($post){
                        return $query->where([['id', '<>', $post->id], ['type', '=', 1]]);
                    })
                ],
                'categories'   => 'required|exists:categories,id',
                'content'      => 'required',
                'status'       => $status,
                'published_at' => $published
            ]);
            $sts = $request->status;
            if (!auth()->user()->can('unpublish posts') && $sts == 4) {
                $sts = $post->status;
            }
            if ($permission) {
                $payloads = $request->except('_token', 'categories', '_method');
                $payloads = removeHtmlTagsOfFields($payloads, 'content');
                $payloads['published_at'] = $sts == 4 ? null : $request->published_at;
            }else{
                $payloads = $request->except('_token', 'categories', '_method', 'published_at', 'status');                
            }
            $payloads['status'] = $sts;
            $html  = str_get_html($request->content);
            $image = null;
            if ($html) {
                $image = $html->find('img', 0);
                $image = empty($image) ? null : basename($image->src);
            }
            $payloads['thumbnail'] = $image;
            $upd  = $post->update($payloads);
            if ($upd) {
                $post->categories()->sync($request->categories);
                return redirect()->route('dashboard.post.index')->with('success', 'post '.$post->title.' updated successfully');
            }
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where(['id' => $id, 'type' => 1])->first();
        if ($post) {
            $delete = $post->delete();
            if ($delete) {
                return redirect()->back()->with('success', 'post '.$post->title.' deleted successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            
            $request->file('upload')->move(public_path('images'), $fileName);
            
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

    private function postScheduleUpdate()
    {
        Post::where('status', 3)->each(function($post){
            if ($post->published_at <= date('Y-m-d H:i:s')) {
                $post->update(['status' => 2]);
            }
        });
    }
}
