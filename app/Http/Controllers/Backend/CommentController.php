<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view comments', ['only' => ['index']]);
        $this->middleware('permission:reply comments', ['only' => ['reply']]);
        $this->middleware('permission:delete comments', ['only' => ['destroy']]);
    }

    public function index()
    {
        $comments = Comment::has('post')->with('post', 'childrens.parent')->whereNull("parent_id")->orderByDesc('created_at')->get();
        return view('backend.comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        if ($comment) {
            $delete = $comment->delete();
            if ($delete) {
                return redirect()->back()->with('success', 'comment '.$comment->name.' deleted successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    public function reply(Request $request)
    {
        $request->validate([
            'post_id'   => 'required|exists:posts,id',
            'parent_id' => 'required|exists:comments,id',
            'reply_to'  => 'required|exists:comments,id',
            'comment'   => 'required'
        ]);
        $user = auth()->user();
        $payloads = $request->except('_token');
        $payloads = removeHtmlTagsOfFields($payloads);
        $payloads['name']  = $user->name;
        $payloads['email'] = $user->email;
        $comment = Comment::create($payloads);
        if ($comment) {
            return redirect()->back()->with('success', 'comment '.$comment->name.' reply successfully');
        }
        return redirect()->back()->with('error', 'failed, Try Again');
    }
}
