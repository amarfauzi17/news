<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $nameRoles = ['Categories', 'Comments', 'Messages', 'Newsletters', 'Pages', 'Posts', 'Roles', 'Users'];

    public function __construct()
    {
        $this->middleware('permission:view roles', ['only' => ['index']]);
        $this->middleware('permission:create roles', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit roles', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete roles', ['only' => ['destroy']]);
    }

    public function index()
    {
        $roles = Role::with('permissions')->where('name', '<>', 'admin')->orderByDesc('created_at')->get();
        $permissions = Permission::orderBy('name')->get();
        $permissionsroles = [];
        foreach ($this->nameRoles as $nameRole) {
            $permissionsroles[$nameRole] = $permissions->map(function($item) use ($nameRole){
                if (Str::contains(strtolower($item->name), strtolower($nameRole))) {
                    return ucwords($item->name);
                } 
            })->filter()->all();
        }
        $nameRoles = $this->nameRoles;
        return view('backend.roles.index', compact('roles', 'permissions', 'permissionsroles', 'nameRoles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
            'permissions' => 'required|array|min:1',
        ]);
        $role = Role::create(['name' => $request->name]);
        if ($role) {
            $role->givePermissionTo($request->permissions);
            return redirect()->back()->with('success', 'roles '.$role->name.' saved successfully');          
        }
        return redirect()->back()->with('error', 'failed, Try Again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findById($id);
        if ($role) {
            $request->validate([
                'name' => 'required|unique:roles,name,'.$role->id,
                'permissions' => 'required|array|min:1',
            ]);
            $upd = $role->update(['name' => $request->name]);
            if ($upd) {
                $role->syncPermissions($request->permissions);
                return redirect()->back()->with('success', 'roles '.$role->name.' updated successfully'); 
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findById($id);
        if ($role) {
            $delete = $role->delete();
            if ($delete) {
                return redirect()->back()->with('success', 'role '.$role->name.' deleted successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }
}
