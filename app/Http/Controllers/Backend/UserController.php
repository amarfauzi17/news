<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view users', ['only' => ['index']]);
        $this->middleware('permission:create users', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit users', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete users', ['only' => ['destroy']]);
        $this->middleware('permission:reset password users', ['only' => ['resetPassword']]);
    }

    public function index()
    {
        $users = User::whereHas('roles', function($query){
            return $query->where('name', '<>', 'admin');
        })->with('roles.permissions')->orderByDesc('created_at')->get();
        $roles = Role::with('permissions')->where('name', '<>', 'admin')->orderByDesc('created_at')->get();
        return view('backend.users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'       => 'required',
            'email'      => 'required|unique:users,email',
            'password'   => 'required|min:6|same:repassword',
            'repassword' => 'required|min:6',
            'roles'      => 'required|exists:roles,name'
        ]);

        $user = User::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => bcrypt($request->password),
        ]);
        if ($user) {
            $user->assignRole($request->roles);
            return redirect()->back()->with('success', 'user '.$user->name.' saved successfully');          
        }
        return redirect()->back()->with('error', 'failed, Try Again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $request->validate([
                'name'  => 'required',
                'email' => 'required|unique:users,email,'.$user->id,
                'roles' => 'required|exists:roles,name'
            ]);

            $upd = $user->update([
                'name'  => $request->name,
                'email' => $request->email,
            ]);
            if ($upd) {
                $user->syncRoles($request->roles);
                return redirect()->back()->with('success', 'user '.$user->name.' saved successfully');          
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $delete = $user->delete();
            if ($delete) {
                return redirect()->back()->with('success', 'user '.$user->name.' deleted successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    public function resetPassword(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $request->validate([
                'password'   => 'required|min:6|same:repassword',
                'repassword' => 'required|min:6',
            ]);

            $upd = $user->update(['password' => bcrypt($request->password)]);
            if ($upd) {
                return redirect()->back()->with('success', 'user '.$user->name.' updated successfully');          
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }
}
