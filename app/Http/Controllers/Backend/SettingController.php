<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('backend.settings.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $user = Auth::user();
        if ($user) {
            $upd = $user->update(['name' => $request->name, 'about' => $request->about]);
            if ($upd) {
                return redirect()->back()->with('success', 'user '.$user->name.' updated successfully');          
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'cpassword' => 'required',
            'npassword' => 'required|same:rpassword|min:6',
            'rpassword' => 'required|min:6',
        ]);
        $user = Auth::user();
        if (Hash::check($request->cpassword, $user->password)) {
            $upd = $user->update(['password' => bcrypt($request->npassword)]);
            if ($upd) {
                return redirect()->back()->with('success', 'user '.$user->name.' updated successfully');          
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }else{
            return redirect()->back()->with('error', 'Old Password didn\'t match');
        }
    }
}
