<?php

namespace App\Http\Views\Composers;

use Illuminate\View\View;
use App\Models\Post;
use App\Models\Category;
use Visitor;

class FrontendComposer
{
	
	private $side_categories = [];
	private $side_posts = [];

	public function __construct()
	{
		$this->side_categories = Category::with(['posts' => function($sql){
			$sql->where([['type', '=', 1], ['published_at', '<=', date('Y-m-d H:i:s')]]);
		}])->orderBy('name')->orderBy('views')->take(10)->get();
		$this->side_posts = Post::where([['published_at', '<=', date('Y-m-d H:i:s')]])->orderByDesc('published_at')->get();
	}

	public function compose(View $view)
    {
    	Visitor()->visit();
        $view->with([
            'side_categories' => $this->side_categories,
            'side_posts' => $this->side_posts,
        ]);
    }


}