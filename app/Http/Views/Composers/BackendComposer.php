<?php

namespace App\Http\Views\Composers;

use Illuminate\View\View;
use App\Models\Post;
use App\Models\Category;
use Auth;

class BackendComposer
{
	
	private $auth;

	public function __construct()
	{
		$this->auth = Auth::user();
	}

	public function compose(View $view)
    {
        $view->with([
            'auth' => $this->auth,
        ]);
    }


}