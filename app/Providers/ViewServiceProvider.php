<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['layouts.frontend'],
            'App\Http\Views\Composers\FrontendComposer'
        );
        // View::composer(
        //     ['layouts.backend'],
        //     'App\Http\Views\Composers\BackendComposer'
        // );
    }
}
