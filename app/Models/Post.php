<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;

class Post extends Model
{
	use HasFactory, Blameable;

	protected $guarded = ['id'];
	protected $casts   = ['published_at' => 'datetime'];

	public function categories()
	{
		return $this->belongsToMany(Category::class)->withTimestamps();
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	public function comments()
	{
		return $this->hasMany(Comment::class);
	}
}
