<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	use HasFactory;

	protected $guarded = ['id'];

	public function post()
	{
		return $this->belongsTo(Post::class);
	}

	public function childrens()
	{
		return $this->hasMany(Comment::class, 'parent_id');
	}
	
	public function parent()
	{
		return $this->belongsTo(Comment::class, 'parent_id');
	}

	public function reply()
	{
		return $this->belongsTo(Comment::class, 'reply_to');
	}

	public function user()
	{
		return $this->belongsTo(Post::class, 'created_by');
	}
}
