<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.post.index');
// });

Auth::routes(['register' => false]);

// frontend
Route::get('/404', function(){
	return view('frontend.errors.404');
})->name('error.404');

Route::get('/', [App\Http\Controllers\Frontend\PostController::class, 'index'])->name('post.index');
Route::get('/pages/{slug}', [App\Http\Controllers\Frontend\PageController::class, 'show'])->name('page.show');
Route::get('/category/{id}', [App\Http\Controllers\Frontend\CategoryController::class, 'show'])->name('category.show');
Route::post('/newsletter', [App\Http\Controllers\Frontend\NewsletterController::class, 'store'])->name('newsletter.store');
Route::resource('/author', App\Http\Controllers\Frontend\AuthorController::class, ['names' => 'author'])->only('index', 'show');
Route::resource('/contact-us', App\Http\Controllers\Frontend\ContactUsController::class, ['names' => 'contact'])->only('index', 'store');
Route::post('/comment', [App\Http\Controllers\Frontend\CommentController::class, 'store'])->name('comment.store');

// backend
Route::group(['as' => 'dashboard.', 'prefix' => 'dashboard', 'middleware' => ['auth']], function (){
	Route::get('/', [App\Http\Controllers\Backend\DashboardController::class, 'index'])->name('index');
	Route::resource('/posts', App\Http\Controllers\Backend\PostController::class, ['names' => 'post'])->except('show');
	Route::post('/posts/upload', [App\Http\Controllers\Backend\PostController::class, 'upload'])->name('post.upload');
	Route::resource('/categories', App\Http\Controllers\Backend\CategoryController::class, ['names' => 'category'])->except('create', 'edit', 'show');
	Route::resource('/message', App\Http\Controllers\Backend\MessageController::class, ['names' => 'message'])->only('index', 'destroy');
	Route::resource('/newsletter', App\Http\Controllers\Backend\NewsletterController::class, ['names' => 'newsletter'])->only('index', 'destroy');
	Route::resource('/comment', App\Http\Controllers\Backend\CommentController::class, ['names' => 'comment']);
	Route::post('/comment/reply', [App\Http\Controllers\Backend\CommentController::class, 'reply'])->name('comment.reply');
	Route::resource('/role', App\Http\Controllers\Backend\RoleController::class, ['names' => 'role'])->except('create', 'edit', 'show');
	Route::resource('/user', App\Http\Controllers\Backend\UserController::class, ['names' => 'user'])->except('create', 'edit', 'show');
	Route::put('/reset-password/{id}', [App\Http\Controllers\Backend\UserController::class, 'resetPassword'])->name('user.reset-password');
	Route::put('/setting/change-password', [App\Http\Controllers\Backend\SettingController::class, 'changePassword'])->name('setting.change-password');
	Route::resource('/setting', App\Http\Controllers\Backend\SettingController::class, ['names' => 'setting'])->only('index', 'update');
	Route::resource('/pages', App\Http\Controllers\Backend\PageController::class, ['names' => 'page']);

	Route::get('/404', function(){
		return view('backend.errors.404');
	})->name('error.404');
});

// post
Route::get('/{slug}', [App\Http\Controllers\Frontend\PostController::class, 'show'])->name('post.show');