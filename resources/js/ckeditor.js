import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import CKFinder from '@ckeditor/ckeditor5-ckfinder/src/ckfinder';
// ref: https://tobiasahlin.com/blog/move-from-jquery-to-vanilla-javascript/#document-ready

var ready = (callback) => {
	if (document.readyState != "loading") callback();
	else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => { 
	ClassicEditor
	.create(document.querySelector('.wysiwyg'), {
		ckfinder: {
			uploadUrl : ''
		}
	})
	.catch(error => {
		console.log(`error`, error)
	});
});