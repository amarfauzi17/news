@if ($paginator->hasPages())
@php
$pageLimit   = 2;
$pageTotal   = ceil($paginator->total() / $paginator->perPage());
$pageCurrent = $paginator->currentPage();
$pageStart   = $pageCurrent - $pageLimit <= 0 ? 1 : $pageCurrent - $pageLimit;
$pageEnd     = $pageCurrent + $pageLimit >= $pageTotal ? $pageTotal : $pageCurrent + $pageLimit;
$pagePath    = rtrim($paginator->path(), '/').'/';
@endphp
<nav aria-label="Page navigation">
	<ul class="pagination justify-content-start">
		@if (!$paginator->onFirstPage())
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"> <i class="fa fa-long-arrow-left"></i> </a></li>
        @endif

        @for($i = $pageStart; $i <= $pageEnd; $i++)
            <li class="page-item"><a class="page-link {{$i == $pageCurrent ? 'active' : ''}}" href="{{ $pagePath.'?page='.$i }}" aria-current="page">{{ $i }}</a></li>
        @endfor

        @if ($pageCurrent != $pageTotal)
            <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"> <i class="fa fa-long-arrow-right"></i> </a></li>
        @endif
	</ul>
</nav>
@endif