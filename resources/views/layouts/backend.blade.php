<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<meta name="robots" content="noindex">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<link rel="stylesheet" href="{{asset("css/backend/awesome.css")}}">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="{{asset("css/backend/adminlte.css")}}">
	@yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		@include('backend.partials.navbar')
		@include('backend.partials.sidebar')
		<div class="content-wrapper">
			@include('backend.partials.breadcrumb')
			<section class="content">
				<div class="container-fluid">
					@include('backend.partials.flash-message')
					@yield('content')
				</div>
			</section>
		</div>
		@include('backend.partials.footer')
	</div>
	<script src="{{asset("js/jquery.js")}}"></script>
	<script src="{{asset("js/backend/jquery-ui.js")}}"></script>
	<script src="{{asset("js/backend/bootstrap.bundle.js")}}"></script>
	<script src="{{asset("js/backend/adminlte.js")}}"></script>
	<script type="text/javascript">
		$('[data-dismiss="alert"]').on('click', function(){
			$(this).parents('div.alert').remove()
		})
	</script>
	@yield('js')
</body>
</html>