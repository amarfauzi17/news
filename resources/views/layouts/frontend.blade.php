<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="canonical" href="{{url()->current()}}" />
    @yield('metatag')
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    @yield('css')
</head>
<body>
    <div id="wrapper">
        <header class="tech-header header">
            <div class="container-fluid">
                @include("frontend.partials.navbar")
            </div>
        </header>
        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                        @include('frontend.partials.flash-message')
                        @yield('content')
                    </div>
                    @include('frontend.partials.sidebar')
                </div>
            </div>
        </section>
        @include("frontend.partials.footer")
    </div>
    <script src="{{asset("js/frontend/jquery.min.js")}}"></script>
    <script src="{{asset("js/frontend/tether.min.js")}}"></script>
    <script src="{{asset("js/frontend/bootstrap.min.js")}}"></script>
    <script src="{{asset("js/frontend/custom.js")}}"></script>
    <script type="text/javascript">
        var cssFiles = [
        '{{asset("css/frontend/bootstrap.css")}}',
        '{{asset('css/frontend/font-awesome.min.css')}}',
        '{{asset('css/frontend/style.css')}}',
        '{{asset('css/frontend/responsive.css')}}',
        '{{asset('css/frontend/colors.css')}}',
        '{{asset('css/frontend/tech.css')}}',
        ];

        cssFiles.forEach(function(css){
            $('<link>')
            .appendTo('head')
            .attr({
                type: 'text/css', 
                rel: 'stylesheet',
                href: css
            });
        });
    </script>
    @yield('js')
</body>
</html>