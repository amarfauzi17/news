@extends("layouts.frontend")
@section('title', 'Contact Us Tech Blog')
@section('content')
<div class="page-wrapper">
    <div class="row">
        <div class="col-lg-5">
            <h4>Who we are</h4>
            <p>Tech Blog is a personal blog for handcrafted, cameramade photography content, fashion styles from independent creatives around the world.</p>

            <h4>How we help?</h4>
            <p>Etiam vulputate urna id libero auctor maximus. Nulla dignissim ligula diam, in sollicitudin ligula congue quis turpis dui urna nibhs. </p>

            <h4>Pre-Sale Question</h4>
            <p>Fusce dapibus nunc quis quam tempor vestibulum sit amet consequat enim. Pellentesque blandit hendrerit placerat. Integertis non.</p>
        </div>
        <div class="col-lg-7">
            <form action="{{route('contact.store')}}" class="form-wrapper" method="POST">
                @csrf
                <input type="text" name="name" class="form-control" placeholder="Your name" autocomplete="off" required>
                <input type="email" name="email" class="form-control" placeholder="Email address" autocomplete="off" required>
                <input type="text" name="website" class="form-control" placeholder="Website" autocomplete="off">
                <input type="text" name="phone" maxlength="15" minlength="8" class="form-control" onkeypress="return isNumber(event)" placeholder="Phone" autocomplete="off">
                <input type="text" name="subject" class="form-control" placeholder="Subject" required autocomplete="off">
                <textarea class="form-control" name="message" placeholder="Your message" required></textarea>
                <button type="submit" class="btn btn-primary">Send <i class="fa fa-envelope-open-o"></i></button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
@endsection