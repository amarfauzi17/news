@extends("layouts.frontend")
@section('title', $page->title)
@section('content')
<div class="page-wrapper">

    <div class="blog-title-area text-center">
        <h3>{{$page->title}}</h3>

        <div class="blog-meta big-meta">
            <small>{{date('d F, Y', strtotime($page->created_at))}}</small>
        </div><!-- end meta -->

        <div class="post-sharing">
            <ul class="list-inline">
                <li><a href="#" class="fb-button btn btn-primary"><i class="fa fa-facebook"></i> <span class="down-mobile">Share on Facebook</span></a></li>
                <li><a href="#" class="tw-button btn btn-primary"><i class="fa fa-twitter"></i> <span class="down-mobile">Tweet on Twitter</span></a></li>
                <li><a href="#" class="gp-button btn btn-primary"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div><!-- end post-sharing -->
    </div><!-- end title -->

    <div class="blog-content">
        {!!$page->content!!}
    </div><!-- end content -->
    <hr class="invis1">
</div>
@endsection