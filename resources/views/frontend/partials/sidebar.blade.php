<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 side-post">
    <div class="sidebar">
        <div class="widget">
            <h2 class="widget-title">Popular Posts</h2>
            <div class="blog-list-widget">
                <div class="list-group">
                    @foreach($side_posts->where('type', 1)->sortByDesc('views')->take(5) as $popular)
                    <a href="{{route('post.show', $popular->slug)}}" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="w-100 justify-content-between">
                            <img src="{{asset("images/posts/".$popular->thumbnail)}}" alt="" class="img-fluid float-left">
                            <h5 class="mb-1">{{$popular->title}}</h5>
                            <small>{{date('d F, Y', strtotime($popular->created_at))}}</small>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div><!-- end blog-list -->
        </div><!-- end widget -->

        <div class="widget">
            <h2 class="widget-title">Recent Reviews</h2>
            <div class="blog-list-widget">
                <div class="list-group">
                    @foreach($side_posts->where('type', 1)->sortByDesc('created_at')->take(3) as $recent)
                    <a href="tech-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="w-100 justify-content-between">
                            <img src="{{asset("images/posts/tech_blog_02.jpg")}}" alt="" class="img-fluid float-left">
                            <h5 class="mb-1">{{Str::limit($recent->title, 30)}}</h5>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div><!-- end blog-list -->
        </div><!-- end widget -->

        <div class="widget">
            <h2 class="widget-title">Categories</h2>
            <div class="tag-cloud-single">
                @foreach($side_categories as $category)
                <a href="{{route('category.show', $category->id)}}" class="text-white"><span class="mt-2">{{$category->name}}</span></a>
                @endforeach
            </div>
        </div>

        {{-- <div class="widget">
            <h2 class="widget-title">Follow Us</h2>

            <div class="row text-center">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <a href="#" class="social-button facebook-button">
                        <i class="fa fa-facebook"></i>
                        <p>27k</p>
                    </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <a href="#" class="social-button twitter-button">
                        <i class="fa fa-twitter"></i>
                        <p>98k</p>
                    </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <a href="#" class="social-button google-button">
                        <i class="fa fa-google-plus"></i>
                        <p>17k</p>
                    </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <a href="#" class="social-button youtube-button">
                        <i class="fa fa-youtube"></i>
                        <p>22k</p>
                    </a>
                </div>
            </div>
        </div> --}}
    </div>
</div>