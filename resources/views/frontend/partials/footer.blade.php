<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="widget">
                    <div class="footer-text text-left">
                        <a href="index.html"><img src="images/version/tech-footer-logo.png" alt="" class="img-fluid"></a>
                        <p>Tech Blog is a technology blog, we sharing marketing, news and gadget articles.</p>
                        <div class="social">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>              
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                        </div>
                        <hr class="invis">
                        <div class="newsletter-widget text-left">
                            <form class="form-inline" method="POST" action="{{route('newsletter.store')}}">
                                @csrf
                                <input type="email" class="form-control" placeholder="Enter your email address" name="email" autocomplete="off" required>
                                <button type="submit" class="btn btn-primary">SUBMIT</button>
                            </form>
                        </div><!-- end newsletter -->
                    </div><!-- end footer-text -->
                </div><!-- end widget -->
            </div><!-- end col -->

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="widget">
                    <h2 class="widget-title">Popular Categories</h2>
                    <div class="link-widget">
                        <ul>
                            @foreach($side_categories->take(5)->sortBy('name') as $category)
                            <li><a href="{{route('category.show', $category->id)}}">{{ucwords($category->name)}} <span>({{$category->posts->count()}} <small>posts</small>)</span></a></li>
                            @endforeach
                        </ul>
                    </div><!-- end link-widget -->
                </div><!-- end widget -->
            </div><!-- end col -->

            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                <div class="widget">
                    <h2 class="widget-title">Pages</h2>
                    <div class="link-widget">
                        <ul>
                            @foreach($side_posts->where('type', 2)->sortBy('title') as $page)
                            <li><a href="{{route('page.show', $page->slug)}}">{{$page->title}}</a></li>
                            @endforeach
                        </ul>
                    </div><!-- end link-widget -->
                </div><!-- end widget -->
            </div><!-- end col -->
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <div class="copyright">&copy; Tech Blog. Design: <a href="http://html.design" rel="nofollow">HTML Design</a>.</div>
            </div>
        </div>
    </div>
</footer>
<div class="dmtop">Scroll to Top</div>