<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="top:5px;">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="tech-index.html"><img src="images/version/tech-logo.png" alt=""></a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/')}}">Home</a>
            </li>
            <li class="nav-item dropdown has-submenu menu-large hidden-md-down hidden-sm-down hidden-xs-down">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">News</a>
                <ul class="dropdown-menu megamenu" aria-labelledby="dropdown01">
                    <li>
                        <div class="container">
                            <div class="mega-menu-content clearfix">
                                <div class="tab">
                                    @foreach($side_categories->take(5)->sortBy('name') as $category)
                                    <button class="tablinks {{$loop->iteration == 1 ? 'active' : ''}}" onclick="openCategory(event, '{{'cat-'.$loop->iteration}}')">{{ucwords($category->name)}}</button>
                                    @endforeach
                                </div>

                                <div class="tab-details clearfix">
                                    @foreach($side_categories->take(5)->sortBy('name') as $category)
                                    <div id="{{"cat-".$loop->iteration}}" class="tabcontent {{$loop->iteration == 1 ? 'active' : ''}}">
                                        <div class="row">
                                            @foreach($category->posts->sortByDesc('views')->take(4) as $post)
                                            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                                <div class="blog-box">
                                                    <div class="post-media">
                                                        <a href="{{route('post.show', $post->slug)}}" title="">
                                                            <img src="{{asset('images/posts/'.$post->thumbnail)}}" alt="" class="img-fluid">
                                                            <div class="hovereffect">
                                                            </div>
                                                            <span class="menucat">{{$category->name}}</span>
                                                        </a>
                                                    </div>
                                                    <div class="blog-meta">
                                                        <h4><a href="{{route('post.show', $post->slug)}}" title="">{{$post->title}}</a></h4>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>                             
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('author.index')}}">Author</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('contact.index')}}">Contact Us</a>
            </li>
            <li class="nav-item">
                @if (Auth::check())
                <a class="nav-link" href="{{route('dashboard.index')}}">Dashboard</a>
                @else
                <a class="nav-link" href="{{url('/login')}}">Login</a>
                @endif
            </li>
        </ul>
        <ul class="navbar-nav mr-2">
            <form action="{{url('/')}}" method="GET">
                <input type="text" name="q" class="form-control search" autocomplete="off" placeholder="Search...">
            </form>
        </ul>
    </div>
</nav>