@extends("layouts.frontend")
@section('title', '404 Tech Blog')
@section('content')
<div class="page-wrapper text-center">
    <div class="mt-5">
        <h4><b>404</b><br>oops! Nothing Was Found<br></h4>
        <p>Sorry but the page you are looking for does not exist, have been removed. name changed or is temporarily unavailable</p>
        <button type="button" class="btn btn-primary" onclick="location.href='{{url('/')}}'">Go To Home</button>
    </div>
</div>
@endsection