@extends("layouts.frontend")
@section('title', 'Tech Blog - Stylish Magazine Blog Template')
@section('content')
<div class="page-wrapper">
    <div class="blog-top clearfix">
        <h4 class="pull-left">Recent News <a href="#"><i class="fa fa-rss"></i></a></h4>
    </div>

    @isset($author)
    <div class="custombox authorbox clearfix">
        <h4 class="small-title">About author</h4>
        <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
            <h4><a href="{{route('author.show', $author->id)}}">{{$author->name}}</a></h4>
            <p>{{$author->about}}</p>
            <div class="topsocial">
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Website"><i class="fa fa-link"></i></a>
            </div>
        </div>
    </div>
    <hr class="invis1">
    @endisset

    <div class="blog-list clearfix">
        @forelse($posts as $post)
        <div class="blog-box row">
            <div class="col-md-4">
                <div class="post-media">
                    <a href="{{route('post.show', $post->slug)}}" class="text-decoration-none">
                        <img src="{{asset("images/posts/".$post->thumbnail)}}" alt="" class="img-fluid">
                        <div class="hovereffect"></div>
                    </a>
                </div>
            </div>
            <div class="blog-meta big-meta col-md-8">
                <h4><a class="text-decoration-none" href="{{route('post.show', $post->slug)}}">{{$post->title}}</a></h4>
                <p>{!! Str::words(strip_tags($post->content), 10,'...')  !!}</p>
                @isset($post->categories->first()->name)
                <small class="firstsmall"><a class="bg-orange" href="tech-category-01.html">{{$post->categories->first()->name}}</a></small>
                @endisset
                <small>{{date('d F, Y', strtotime($post->created_at))}}</small>
                <small><a href="#">by {{$post->user->name ?? 'Admin'}}</a></small>
                <small><i class="fa fa-eye"></i> {{$post->views}}</small>
            </div>
        </div>
        <hr class="invis">
        @empty
        <div class="blog-box row">
            <p class="text-center"><i>No Items</i></p>
        </div>
        <hr class="invis">
        @endforelse
    </div>
</div>

<hr class="invis">

<div class="row">
    <div class="col-md-12">
        {{$posts->links("vendor.pagination.custom")}}
    </div>
</div>
@endsection