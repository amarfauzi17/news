@extends("layouts.frontend")
@section('title', $post->title)
@section('metatag')
<meta property="og:url" content="{{$url}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->meta_description}}" />
<meta property="og:image" content="{{asset('images/posts/'.$post->thumbnail)}}" />
@endsection
@section('css')
<style type="text/css">
    .comments-list-sub{
        margin-left: 60px;
    }
    .comments-list .media{
        margin-bottom: 0px !important;
    }
    .replyed-to{
        color:#73658d;
        font-size:10px;
    }
    .fs-13{
        font-size: 13px;
    }
</style>
@endsection
@section('content')
<div class="page-wrapper">
    <div class="blog-title-area text-center">
        <ol class="breadcrumb hidden-xs-down">
            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
            <li class="breadcrumb-item active">{{$post->title}}</li>
        </ol>
        <h3>{{$post->title}}</h3>
        <div class="blog-meta big-meta">
            <small>{{date('d F, Y', strtotime($post->created_at))}}</small>
            <small><a href="{{route('author.show', $post->user->id ?? '99')}}" title="">by {{$post->user->name ?? 'Admin'}}</a></small>
            <small><i class="fa fa-eye"></i> {{$post->views}}</small>
        </div>

        <div class="post-sharing">
            <ul class="list-inline">
                <li><a href="https://www.facebook.com/share.php?u={{$url}}" target="_blank" class="fb-button btn btn-primary"><i class="fa fa-facebook"></i> <span class="down-mobile">Share on Facebook</span></a></li>
                <li><a href="https://twitter.com/intent/tweet?url={{$url}}" target="_blank" class="tw-button btn btn-primary"><i class="fa fa-twitter"></i> <span class="down-mobile">Tweet on Twitter</span></a></li>
                {{-- <li><a href="#" class="gp-button btn btn-primary"><i class="fa fa-google-plus"></i></a></li> --}}
            </ul>
        </div><!-- end post-sharing -->
    </div><!-- end title -->

    <div class="blog-content">
        {!!$post->content!!}
    </div><!-- end content -->

    <div class="blog-title-area">
        <div class="tag-cloud-single">
            <span>Categories</span>
            @foreach($post->categories as $category)
            <small><a href="#" title="">{{$category->name}}</a></small>
            @endforeach
        </div><!-- end meta -->

        <div class="post-sharing">
            <ul class="list-inline">
                <li><a href="#" class="fb-button btn btn-primary"><i class="fa fa-facebook"></i> <span class="down-mobile">Share on Facebook</span></a></li>
                <li><a href="#" class="tw-button btn btn-primary"><i class="fa fa-twitter"></i> <span class="down-mobile">Tweet on Twitter</span></a></li>
                <li><a href="#" class="gp-button btn btn-primary"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div><!-- end post-sharing -->
    </div><!-- end title -->

    <hr class="invis1">

    <div class="custombox authorbox clearfix">
        <h4 class="small-title">About author</h4>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4><a href="{{route('author.show', $post->user->id ?? '99')}}">{{$post->user->name ?? 'Admin'}}</a></h4>
                <p class="text-justify">{{$post->user->about ?? ''}}</p>

                <div class="topsocial">
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Website"><i class="fa fa-link"></i></a>
                </div>
            </div>
        </div>
    </div>

    <hr class="invis1">

    <div class="custombox clearfix">
        <h4 class="small-title">You may also like</h4>
        <div class="row">
            @foreach($relateds as $related)
            <div class="col-lg-6">
                <div class="blog-box">
                    <div class="post-media">
                        <a href="{{route('post.show', $related->slug)}}" title="">
                            <img src="{{asset("images/posts/".$related->thumbnail)}}" alt="" class="img-fluid">
                            <div class="hovereffect">
                                <span class=""></span>
                            </div>
                        </a>
                    </div>
                    <div class="blog-meta">
                        <h4><a href="{{route('post.show', $related->slug)}}" title="">{{$related->title}}</a></h4>
                        <small>{{date('d F, Y', strtotime($related->created_at))}}</small>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <hr class="invis1">

    <div class="custombox clearfix">
        <h4 class="small-title" id="comment-total">0 Comments</h4>
        <div class="row">
            <div class="col-lg-12">
                @php $totalComment = 0; @endphp
                @foreach($post->comments as $comment)
                @php $totalComment++; @endphp
                <div class="comments-list" id="comment-{{$comment->id}}">
                    <div class="media">
                        <div class="media-body">
                            <h4 class="media-heading user_name">{{$comment->name}}&nbsp;&nbsp;&nbsp;<span class="replyed-to">{{$post->created_at->diffForHumans()}}</span></h4>
                            <p>{{$comment->comment}}</p>
                            <a href="javascript:;" data-parent-id="{{$comment->id}}" data-reply-name="{{$comment->name}}" data-reply="{{$comment->id}}" class="comment-form btn btn-primary btn-sm">Reply</a>
                        </div>
                    </div>
                    @foreach($comment->childrens as $subcomment)
                    @php $totalComment++; @endphp                
                    <div class="comments-list-sub" id="comment-{{$subcomment->id}}">
                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading user_name">{{$subcomment->name}}&nbsp;&nbsp;&nbsp;<span class="replyed-to">Replyed to {{$subcomment->reply->name ?? ''}}</span> <small>{{$post->created_at->diffForHumans()}}</small></h4>
                                <p>{{$subcomment->comment}}</p>
                                <a href="javascript:;" data-parent-id="{{$comment->id}}" data-reply-name="{{$subcomment->name}}" data-reply-id="{{$subcomment->id}}" class="comment-form btn btn-primary btn-sm">Reply</a>
                            </div>
                        </div>
                    </div>                    
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <hr class="invis1">

    <div class="custombox clearfix" id="comment-form">
        <h4 class="small-title">Leave a Reply</h4>
        <div class="row">
            <div class="col-lg-12">
                <form class="form-wrapper mt-3" method="POST" action="{{route('comment.store')}}">
                    @csrf
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                    <input type="hidden" name="parent_id">
                    <input type="hidden" name="reply_to">
                    <input type="text" class="form-control" placeholder="Your name" name="name" required>
                    <input type="email" class="form-control" placeholder="Email address" name="email" required>
                    <input type="text" class="form-control" placeholder="Website" name="website">
                    <textarea class="form-control" placeholder="Your comment" name="comment" required></textarea>
                    <button type="submit" class="btn btn-primary">Submit Comment</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#comment-total').html({{$totalComment}}+' comments');
    })

    $("a.comment-form").click(function() {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#comment-form").offset().top - 100
        }, 200);
        var formComment = $('#comment-form');
        var parent_id   = $(this).data('parent-id');
        var replyName   = $(this).data('reply-name');
        var replyId   = $(this).data('reply-id');

        $('input[name="parent_id"]').val(parent_id);
        $('input[name="reply_to"]').val(replyId);

        formComment.find('h4.small-title').html('Leave a Reply to <span class="text-primary">'+replyName+'</span><a class="text-danger fs-13" onclick="cancelReplyTo(this)">&nbsp;&nbsp;&nbsp;Cancel</a>');
    });

    function cancelReplyTo(self)
    {
        var formComment = $(self).parents('#comment-form');
        $('input[name="parent_id"]').val("");
        $('input[name="reply_to"]').val("");
        formComment.find('h4.small-title').html('Leave a Reply');
    }
</script>
@endsection