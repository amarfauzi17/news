@extends("layouts.frontend")
@section('title', 'Authors - Stylish Magazine Blog Template')
@section('content')
<div class="page-wrapper">
    @foreach($authors as $author)
    <div class="custombox authorbox clearfix">
        <h4 class="small-title">About author</h4>
        <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
            <h4><a href="{{route('author.show', $author->id)}}">{{$author->name}}</a></h4>
            <p>{{$author->about}}</p>
            <div class="topsocial">
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Website"><i class="fa fa-link"></i></a>
            </div>
        </div>
    </div>
    {{-- <hr class="invis1"> --}}
    @endforeach
</div>
<hr class="invis">
@endsection