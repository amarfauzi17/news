@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Setting Index')
@section('css')
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="profile-tab" onclick="tabPanel(this)" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="password-tab" onclick="tabPanel(this)" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="true">Password</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Profile User</h3>
							</div>
							<form action="{{route('dashboard.setting.update', 'user')}}" method="POST">
								@csrf
								@method('PUT')
								<div class="card-body">
									<div class="form-group">
										<label>Name</label>
										<input type="text" name="name" value="{{$user->name}}" class="form-control" required>
									</div>
									<div class="form-group">
										<label>About Me</label>
										<textarea class="form-control" rows="3" name="about">{{$user->about}}</textarea>
									</div>
								</div>
								<div class="card-footer">
									<button type="button" onclick="location.href='{{ url()->previous() }}'" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
									<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
								</div>
							</form>
						</div>
					</div>
					<div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Profile User</h3>
							</div>
							<form action="{{route('dashboard.setting.change-password')}}" method="POST">
								@csrf
								@method('PUT')
								<div class="card-body">
									<div class="form-group">
										<label>Current Password</label>
										<input type="password" name="cpassword" class="form-control" required>
									</div>
									<div class="form-group">
										<label>New Password</label>
										<input type="password" name="npassword" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Re Password</label>
										<input type="password" name="rpassword" class="form-control" required>
									</div>
								</div>
								<div class="card-footer">
									<button type="button" onclick="location.href='{{ url()->previous() }}'" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
									<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script type="text/javascript">
	function tabPanel(self)
	{
		var tab   = $('#myTab'); 
		var panel = $('#myTabContent');

		tab.find('li a.active').removeClass('active');
		$(self).addClass('active');
		var url = $(self).attr('href');
		panel.find('div.active').removeClass('active');
		panel.find('div.show').removeClass('show');
		panel.find('div'+url).addClass('active');
		panel.find('div'+url).addClass('show');
	}
</script>
@endsection