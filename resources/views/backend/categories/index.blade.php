@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Category Index')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						@can('create categories')
						<button type="button" onclick="$('#modal-create').modal('show')" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create</button>
						@endcan
						&nbsp;&nbsp;&nbsp;&nbsp;
						<h3 class="card-title float-right">DataTable Categories</h3>
					</div>
					<div class="card-body">
						<table id="tbl-category" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="10%">No</th>
								<th>Category</th>
								@canany(['edit categories', 'delete categories'])
								<th width="15%"><center>Action</center></th>
								@endcan
							</thead>
							<tbody>
								@foreach($categories as $category)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$category->name}}</td>
									@canany(['edit categories', 'delete categories'])
									<td class="text-nowrap">
										<center>
											@can('edit categories')
											<button class="btn btn-sm btn-warning" data-id="{{$category->id}}" data-title="{{$category->name}}" onclick="modalEdit(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-pencil"></i></button>
											&nbsp;&nbsp;
											@endcan
											@can('delete categories')
											<button class="btn btn-sm btn-danger" data-id="{{$category->id}}" data-title="{{$category->name}}" onclick="modalDelete(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
											@endcan
										</center>
									</td>
									@endcan
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@can('create categories')
<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="{{route('dashboard.category.store')}}">
				@csrf
				<div class="modal-header bg-primary">
					<h4 class="modal-title">Create Category</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<label>Name</label>
					<input type="text" name="name" class="form-control" required>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit categories')
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-warning">
					<h4 class="modal-title">Edit Category</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<label>Name</label>
					<input type="text" name="name" class="form-control" required>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-warning">Upadte</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete categories')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete Category</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete category <b id="modal-delete-category-title"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-category').DataTable();
	});

	@can('edit categories')
	function modalEdit(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-edit');
		var url   = '{{ route("dashboard.category.update", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('input[name="name"]').val($(self).attr('data-title'));
		modal.modal('show');
	}
	@endcan

	@can('delete categories')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.category.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-category-title').html('('+$(self).attr('data-title')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection