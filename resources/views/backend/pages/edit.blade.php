@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Post Edit')
@section('css')
<style type="text/css">
	.ck-editor__editable_inline {
		min-height: 500px;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Edit Pages</h3>
					</div>
					<form action="{{route('dashboard.page.update', $page->id)}}" method="POST">
						@csrf
						@method('PUT')
						<div class="card-body">
							<div class="form-group">
								<label>Title</label>
								<input type="text" name="title" required value="{{old('title', $page->title)}}" class="form-control" onkeyup="str_slug(this)">
							</div>
							<div class="form-group">
								<label>Slug</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">{{(request()->secure() ? 'https://' : 'http://').request()->getHost()}}/pages/</span>
									</div>
									<input type="text" class="form-control" value="{{old('slug', $page->slug)}}" aria-describedby="basic-addon3" required name="slug" readonly>
									<div class="input-group-prepend">
										<button class="btn btn-warning" onclick="edit_slug(this)" type="button"><i class="fa fa-pencil"></i> Edit</button>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Content</label>
								<textarea rows="7" class="form-control" name="content" id="content" required>{{old('content', $page->content)}}</textarea>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" onclick="location.href='{{ url()->previous() }}'" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
							<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		CKEDITOR.replace('content', {
			filebrowserUploadUrl: "{{route('dashboard.post.upload', ['_token' => csrf_token() ])}}",
			filebrowserUploadMethod: 'form',
			height: '500px',
			disallowedContent: 'script; *[on*]',
			removeDialogTabs: 'image:info;image:Link;image:advanced',
		});

	});

	function str_slug(self){
		var title = $(self).val();
		title = title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
		$('input[name="slug"]').val(title);
	}

	function edit_slug(self)
	{
		var btn  = $(self);
		var slug = $('input[name="slug"]');
		if(btn.hasClass('btn-warning')) {
			slug.attr('readonly', false);
			btn.toggleClass('btn-warning btn-success');
			btn.html('<i class="fa fa-save"></i> Save');
			$('input[name="title"]').attr('onkeyup', '');
		} else {
			slug.attr('readonly', true);
			btn.toggleClass('btn-success btn-warning');
			btn.html('<i class="fa fa-pencil"></i> Edit');
			$('input[name="title"]').attr('onkeyup', 'str_slug(this)');
		}
	}
</script>
@endsection