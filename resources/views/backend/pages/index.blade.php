@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Page Index')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-pages_paginate, #tbl-pages_filter{
		float: right;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">DataTable Pages</h3>
					</div>
					<div class="card-body">
						<table id="tbl-pages" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="5%">No</th>
								<th>Title</th>
								<th width="10%">Action</th>
							</thead>
							<tbody>
								@foreach($pages as $page)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$page->title}}</td>
									<td class="text-nowrap">
										@can('edit pages')
										<a href="{{route('dashboard.page.edit', $page->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
										&nbsp;&nbsp;
										@endcan
										@can('delete pages')
										<button class="btn btn-sm btn-danger" data-id="{{$page->id}}" data-title="{{$page->title}}" onclick="modalDelete(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
										&nbsp;&nbsp;
										@endcan
										<a href="{{route('page.show', $page->slug)}}" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@can('delete pages')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete Page</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete page <b id="modal-delete-page-title"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-pages').DataTable();
	});

	@can('delete pages')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.page.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-page-title').html('('+$(self).attr('data-title')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection