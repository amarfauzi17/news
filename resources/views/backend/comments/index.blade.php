@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Comment View')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">DataTable comments</h3>
					</div>
					<div class="card-body">
						<table id="tbl-comment" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="5%"><center>No</center></th>
								<th>Name</th>
								<th>Email</th>
								<th width="40%">Comment</th>
								<th>Post Title</th>
								<th width="10%"><center>#</center></th>
							</thead>
							<tbody>
								@foreach($comments as $comment)
								<tr>
									<td rowspan="{{$comment->childrens->count() + 1}}">{{$loop->iteration}}</td>
									<td>{{$comment->name}}</td>
									<td>{{$comment->email}}</td>
									<td>{{$comment->comment}}</td>
									<td>{{$comment->post->title ?? ''}}</td>
									<td>
										<center>
											@can('reply comments')
											<button type="button" class="btn btn-xs btn-outline-primary" data-parent_id="{{empty($comment->parent_id) ? $comment->id : $comment->parent_id}}" data-reply="{{$comment->id}}" data-title="{{$comment->post->title ?? ''}}" data-comment="{{$comment->comment}}" data-post_id="{{$comment->post->id ?? ''}}" onclick="modalReply(this)"><i class="fa fa-reply"></i></button>
											&nbsp;
											@endcan
											@can('delete comments')
											<button class="btn btn-xs btn-outline-danger" onclick="modalDelete(this)" data-id="{{$comment->id}}" data-name="{{$comment->email}}" type="button"><i class="fa fa-trash"></i></button>
											&nbsp;
											@endcan
											<a class="btn btn-xs btn-outline-info" href="{{route('post.show', $comment->post->slug).'#comment-'.$comment->id}}" target="_blank"><i class="fa fa-eye"></i></a>
										</center>
									</td>
								</tr>
								@foreach($comment->childrens as $children)
								<tr class="text-primary">
									<td class="d-none">#</td>
									<td>{{$children->name}}</td>
									<td>{{$children->email}}</td>
									<td>{{$children->comment}}</td>
									<td>Replyed To : {{$children->parent->name ?? ''}}</td>
									<td>
										<center>
											@can('reply comments')
											<button type="button" class="btn btn-xs btn-outline-primary" data-parent_id="{{empty($children->parent_id) ? $children->id : $children->parent_id}}" data-reply="{{$children->id}}" data-title="{{$children->post->title ?? ''}}" data-comment="{{$children->comment}}" data-post_id="{{$children->post->id ?? ''}}" onclick="modalReply(this)"><i class="fa fa-reply"></i></button>
											&nbsp;
											@endcan
											@can('delete comments')
											<button class="btn btn-xs btn-outline-danger" onclick="modalDelete(this)" data-id="{{$children->id}}" data-name="{{$children->email}}" type="button"><i class="fa fa-trash"></i></button>
											&nbsp;
											@endcan
											<a class="btn btn-xs btn-outline-info" href="{{route('post.show', $children->post->slug).'#comment-'.$children->id}}" target="_blank"><i class="fa fa-eye"></i></a>
										</center>
									</td>
								</tr>
								@endforeach
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@can('reply comments')
<div class="modal fade" id="modal-reply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="{{route('dashboard.comment.reply')}}" method="POST">
				@csrf
				<input type="hidden" name="post_id">
				<input type="hidden" name="parent_id">
				<input type="hidden" name="reply_to">
				<div class="modal-header bg-primary">
					<h4 class="modal-title">Reply comment</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form group">
						<label>Title</label>
						<input type="text" class="form-control" disabled id="reply-title">
					</div>
					<div class="form group">
						<label>Comment</label>
						<textarea class="form-control" rows="3" id="reply-comment" disabled></textarea>
					</div>
					<div class="form group">
						<label>Reply</label>
						<textarea class="form-control" rows="3" name="comment" required></textarea>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@can('delete comments')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete comment</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete comment <b id="modal-delete-comment"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-comment').DataTable({
			"ordering": false
		});
	});
	
	@can('reply comments')
	function modalReply(self)
	{
		var modal     = $('#modal-reply');
		var parent_id = $(self).data('parent_id');
		var reply 	  = $(self).data('reply');
		var title     = $(self).data('title');
		var comment   = $(self).data('comment');
		var post_id   = $(self).data('post_id');

		$('input[name="post_id"]').val(post_id);
		$('input[name="parent_id"]').val(parent_id);
		$('input[name="comment"]').val(comment);
		$('input[name="reply_to"]').val(reply);
		$('#reply-comment').val(comment);
		$('#reply-title').val(title);
		modal.modal('show');
	}
	@endcan

	@can('delete comments')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.comment.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-comment').html('('+$(self).attr('data-name')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection