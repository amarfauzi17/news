@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Role Index')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
	.list-roles{
		-webkit-column-count: 3;
		-moz-column-count: 3;
		column-count: 3;
		list-style-type: none;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						@can('create roles')
						<button type="button" onclick="$('#modal-create').modal('show')" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create</button>
						&nbsp;&nbsp;&nbsp;&nbsp;
						@endcan
						<h3 class="card-title float-right">DataTable Roles</h3>
					</div>
					<div class="card-body">
						<table id="tbl-role" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="5%">No</th>
								<th>Role</th>
								<th>Permission</th>
								@canany(['edit roles', 'delete roles'])
								<th width="5%">Action</th>
								@endcan
							</thead>
							<tbody>
								@foreach($roles as $role)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$role->name}}</td>
									@if($role->permissions->count() > 0)
									<td>
										@php
										$items = [];
										foreach ($nameRoles as $nameRole) {
											$items[$nameRole] = $role->permissions->map(function($item) use ($nameRole){
												if (Str::contains(strtolower($item->name), strtolower($nameRole))) {
													return ucwords($item->name);
												} 
											})->filter()->all();
										}
										$items = array_filter($items);
										@endphp
										<ul style="padding-left: 15px;list-style-type: none;">
											@foreach($items as $name => $item)
											<li class="mt-2">
												<span class="text-primary">{{$name}}</span>
												<br>
												@foreach($item as $v)
												<span class="badge badge-secondary mr-2 mt-2 p-1">{{ucwords($v)}}</span>
												@endforeach
											</li>
											@endforeach
										</ul>
									</td>
									@else
									<td><center><i>No Item</i></center></td>
									@endif
									@canany(['edit roles', 'delete roles'])
									<td class="text-nowrap">
										<button class="btn btn-sm btn-warning" data-id="{{$role->id}}" data-title="{{$role->name}}" data-roles="{{$role->permissions->pluck('name')->toJson()}}" onclick="modalEdit(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-edit"></i></button>
										&nbsp;&nbsp;
										<button class="btn btn-sm btn-danger" data-id="{{$role->id}}" data-title="{{$role->name}}" onclick="modalDelete(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
									</td>
									@endcan
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@can('create roles')
<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="{{route('dashboard.role.store')}}">
				@csrf
				<div class="modal-header bg-primary">
					<h4 class="modal-title">Create Role</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Permissions</label>
						<br>
						<ul style="list-style-type: none;">
							@foreach($permissionsroles as $name => $permissionsrole)
							<li class="border-top py-2">
								<b>{{$name}}</b>
								<ul class="list-roles">
									@foreach($permissionsrole as $permission)
									<li>
										<div class="form-check">
											<label class="form-check-label"><input class="form-check-input" data-role="{{strtolower($name)}}" onclick="setPermissions(this)" type="checkbox" name="permissions[]" value="{{strtolower($permission)}}"> {{ucwords($permission)}}</label>
										</div>
									</li>
									@endforeach
								</ul>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit roles')
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-warning">
					<h4 class="modal-title">Edit role</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Permissions</label>
						<br>
						<ul style="list-style-type: none;">
							@foreach($permissionsroles as $name => $permissionsrole)
							<li class="border-top py-2">
								<b>{{$name}}</b>
								<ul class="list-roles">
									@foreach($permissionsrole as $permission)
									<li>
										<div class="form-check">
											<label class="form-check-label"><input class="form-check-input" data-role="{{strtolower($name)}}" onclick="setPermissions(this)" type="checkbox" name="permissions[]" value="{{strtolower($permission)}}"> {{ucwords($permission)}}</label>
										</div>
									</li>
									@endforeach
								</ul>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-warning">Upadte</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete roles')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete role</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete role <b id="modal-delete-role"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-role').DataTable();
	});

	$("#modal-create, #modal-edit").find("form").submit(function(){
		if ($(this).find('input:checkbox').filter(':checked').length < 1){
			alert("Please Check at least one Check Box");
			return false;
		}else{
			$(this).find('input:checkbox').removeAttr('disabled');
		}
	});

	@can('edit roles')
	function modalEdit(self)
	{
		var id    = $(self).attr('data-id');
		var roles = $(self).attr('data-roles');
		var modal = $('#modal-edit');
		var url   = '{{ route("dashboard.role.update", ":id") }}';
		url = url.replace(':id', id);
		roles = JSON.parse(roles);
		roles.forEach(function(name){
			modal.find('input[type="checkbox"][value="'+name+'"]').prop('checked', 'checked');
		});
		modal.find('input[type="checkbox"][value*="view"]:checked').each(function(){
			var role = $(this).data('role');
			if (modal.find('input[type="checkbox"][data-role="'+role+'"][value!="view '+role+'"]:checked').length < 1) {
				modal.find('input[type="checkbox"][value="view '+role+'"]').removeAttr('disabled');
			}else{
				modal.find('input[type="checkbox"][value="view '+role+'"]').attr('disabled', true);		
			}
		});
		modal.find('form').attr('action', url);
		modal.find('input[name="name"]').val($(self).attr('data-title'));
		modal.modal('show');
	}
	@endcan

	@can('delete roles')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.role.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-role').html('('+$(self).attr('data-title')+')');
		modal.modal('show');
	}
	@endcan
	
	function setPermissions(self)
	{
		var value = $(self).val();
		var role  = $(self).data('role');
		var modal = $(self).parents('.modal');
		if ($(self).is(":checked")) {
			if (!value.includes('view')) {
				modal.find('input[type="checkbox"][value="view '+role+'"]').attr('disabled', true);
				modal.find('input[type="checkbox"][value="view '+role+'"]').prop('checked', 'checked');
			}
		}else{
			if (modal.find('input[type="checkbox"][data-role="'+role+'"]:not(:disabled)').filter(':checked').length < 1) {
				modal.find('input[type="checkbox"][value="view '+role+'"]').removeAttr('disabled');
			}
		}
	}
</script>
@endsection