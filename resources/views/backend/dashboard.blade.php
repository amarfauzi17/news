@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Dashboard')
@section('content')
<div class="row">
	<div class="col-lg-3 col-6">
		<div class="small-box bg-info">
			<div class="inner">
				<h3>{{$count_posts}}</h3>
				<p>Posts</p>
			</div>
			<div class="icon">
				<i class="ion ion-page"></i>
			</div>
			<a href="{{route('dashboard.post.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>

	<div class="col-lg-3 col-6">

		<div class="small-box bg-success">
			<div class="inner">
				<h3>{{ $count_views->count() }}</h3>
				<p>Views</p>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>

{{-- 	<div class="col-lg-3 col-6">

		<div class="small-box bg-warning">
			<div class="inner">
				<h3>44</h3>
				<p>User Registrations</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	--}}
	<div class="col-lg-3 col-6">
		<div class="small-box bg-danger">
			<div class="inner">
				<h3>{{ $count_views->count(DB::raw('DISTINCT ip')); }}</h3>
				<p>Unique Visitors</p>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>

<div class="row">
	<section class="col-lg-7 connectedSortable">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">Visitor Chart</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
					</button>
					<button type="button" class="btn btn-tool" data-card-widget="remove">
						<i class="fas fa-times"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">
					<canvas id="myChart" height="150"></canvas>
				</div>
			</div>
		</div>
	</section>
	<section class="col-lg-5 connectedSortable">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">Top Post by Views</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
					</button>
					<button type="button" class="btn btn-tool" data-card-widget="remove">
						<i class="fas fa-times"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
				<table class="table table-responsive table-striped table-bordered">
					<thead>
						<tr>
							<th width="5%"><center>No</center></th>
							<th><center>Title</center></th>
							<th width="5%"><center>View</center></th>
						</tr>
					</thead>
					<tbody>
						@foreach($top_posts as $post)
						<tr>
							<td><center>{{$loop->iteration}}</center></td>
							<td>{{$post->title}}</td>
							<td><center>{{$post->total}}</center></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.min.js"></script>
<script>
	const ctx = document.getElementById('myChart').getContext('2d');
	const myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: {!!json_encode($visits->keys())!!},
			datasets: [
			{
				label: '# Views',
				data: {!! json_encode($visits->map(function($item){return $item->count();})) !!},
				borderColor: [
				'rgba(255, 99, 132, 1)'
				],
				borderWidth: 1
			},
			{
				label: '# Visitors',
				data: {!! json_encode($visits->map(function($item){return $item->groupBy('ip')->count();})) !!},
				borderColor: ['rgba(0, 123, 255, 1)'],
				borderWidth: 1
			}
			]
		},
		options: {
			scales: {
				y: {
					beginAtZero: true
				}
			}
		}
	});
</script>
@endsection