@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Post Create')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/select2.css')}}">
<style type="text/css">
	.select2-container--default .select2-selection--single{
		height: 38px;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice{
		color:#000;
	}
	.ck-editor__editable_inline {
		min-height: 500px;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Create Posts</h3>
					</div>
					<form action="{{route('dashboard.post.store')}}" method="POST">
						@csrf
						<div class="card-body">
							<div class="form-group">
								<label>Title</label>
								<input type="text" name="title" required value="{{old('title')}}" class="form-control" onkeyup="str_slug(this)">
							</div>
							<div class="form-group">
								<label>Slug</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">{{(request()->secure() ? 'https://' : 'http://').request()->getHost()}}/</span>
									</div>
									<input type="text" class="form-control" value="{{old('slug')}}" aria-describedby="basic-addon3" required name="slug" readonly>
									<div class="input-group-prepend">
										<button class="btn btn-warning" onclick="edit_slug(this)" type="button"><i class="fa fa-pencil"></i> Edit</button>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Categories</label>
								<select class="form-control select2" name="categories[]" multiple required>
									@foreach($categories as $category)
									<option value="{{$category->id}}" {{old('categories') == $category->id ? 'selected' : ''}}>{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Content</label>
								<textarea rows="7" class="form-control" name="content" id="content" required>{{old('content')}}</textarea>
							</div>
							<div class="form-group">
								<label>Status</label>
								<select class="form-control" onchange="getDate(this)" name="status" required>
									<option value="1" {{old('status') == 1 ? 'selected' : ''}}>Draft</option>
									<option value="2" {{old('status') == 2 ? 'selected' : ''}} {{ Auth::user()->can('permission:publish posts') ? '' : 'disabled' }}>Published</option>
									<option value="3" {{old('status') == 3 ? 'selected' : ''}} {{ Auth::user()->can('permission:publish posts') ? '' : 'disabled' }}>Schedule</option>
								</select>
							</div>
							<div class="form-group d-none">
								<label>Date</label>
								<input type="datetime-local" class="form-control" name="published_at" min="{{date('Y-m-d\TH:i')}}" value="{{old('published_at')}}">
							</div>
							<div class="form-group">
								<label>Meta Title</label>
								<input type="text" class="form-control" name="meta_title" value="{{old('meta_title')}}">
							</div>
							<div class="form-group">
								<label>Meta Description</label>
								<textarea name="meta_description" rows="2" class="form-control">{{old('meta_description')}}</textarea>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" onclick="location.href='{{ url()->previous() }}'" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
							<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/select2.js')}}"></script>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
		CKEDITOR.replace('content', {
			filebrowserUploadUrl: "{{route('dashboard.post.upload', ['_token' => csrf_token() ])}}",
			filebrowserUploadMethod: 'form',
			height: '500px',
			disallowedContent: 'script; *[on*]',
			removeDialogTabs: 'image:info;image:Link;image:advanced',
		});

	});

	function str_slug(self){
		var title = $(self).val();
		title = title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
		$('input[name="slug"]').val(title);
	}

	function edit_slug(self)
	{
		var btn  = $(self);
		var slug = $('input[name="slug"]');
		if(btn.hasClass('btn-warning')) {
			slug.attr('readonly', false);
			btn.toggleClass('btn-warning btn-success');
			btn.html('<i class="fa fa-save"></i> Save');
			$('input[name="title"]').attr('onkeyup', '');
		} else {
			slug.attr('readonly', true);
			btn.toggleClass('btn-success btn-warning');
			btn.html('<i class="fa fa-pencil"></i> Edit');
			$('input[name="title"]').attr('onkeyup', 'str_slug(this)');
		}
	}

	function getDate(self)
	{
		var date = $('input[name="published_at"]');
		if ($(self).val() == 3) {
			date.parents('div.form-group').removeClass('d-none');
			date.attr('required', true);
		}else{
			date.parents('div.form-group').addClass('d-none');
			date.attr('required', false);
			date.val('');
		}
	}
</script>
@endsection