@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Post Index')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">DataTable Posts</h3>
					</div>
					<div class="card-body">
						<table id="tbl-posts" class="table table-bordered table-hover dataTable">
							<thead>
								<th>No</th>
								<th>Title</th>
								<th>Categories</th>
								<th>Status</th>
								{{-- <th>Views</th> --}}
								<th>Action</th>
							</thead>
							<tbody>
								@foreach($posts as $post)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$post->title}}</td>
									<td>
										@forelse($post->categories as $category)
										<span class="badge badge-success p-2 mt-1">{{$category->name}}</span>
										@empty
										<i  class="text-center">Empty</i>
										@endforelse
									</td>
									<td>{{$status[$post->status]}}</td>
									{{-- <td>{{$post->views}}</td> --}}
									<td class="text-nowrap">
										@can('edit posts')
										<a href="{{route('dashboard.post.edit', $post->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
										&nbsp;&nbsp;
										@endcan
										@can('delete posts')
										<button class="btn btn-sm btn-danger" data-id="{{$post->id}}" data-title="{{$post->title}}" onclick="modalDelete(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
										&nbsp;&nbsp;
										@endcan
										@if($post->status == 2)
										<a href="{{route('post.show', $post->slug)}}" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i></a>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@can('delete posts')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete Post</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete post <b id="modal-delete-post-title"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-posts').DataTable();
	});

	@can('delete posts')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.post.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-post-title').html('('+$(self).attr('data-title')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection