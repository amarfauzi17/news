@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Post Edit')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/select2.css')}}">
<style type="text/css">
	.select2-container--default .select2-selection--single{
		height: 38px;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice{
		color:#000;
	}
	.ck-editor__editable_inline {
		min-height: 500px;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Edit Posts</h3>
					</div>
					<form action="{{route('dashboard.post.update', $post->id)}}" method="POST">
						@csrf
						@method('PUT')
						<div class="card-body">
							<div class="form-group">
								<label>Title</label>
								<input type="text" name="title" required value="{{old('title', $post->title)}}" class="form-control" onkeyup="str_slug(this)">
							</div>
							<div class="form-group">
								<label>Slug</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">{{(request()->secure() ? 'https://' : 'http://').request()->getHost()}}/</span>
									</div>
									<input type="text" class="form-control" value="{{old('slug', $post->slug)}}" aria-describedby="basic-addon3" required name="slug" readonly>
									<div class="input-group-prepend">
										<button class="btn btn-warning" onclick="edit_slug(this)" type="button"><i class="fa fa-pencil"></i> Edit</button>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Categories</label>
								<select class="form-control select2" name="categories[]" multiple required>
									@foreach($categories as $category)
									<option value="{{$category->id}}" {{ in_array($category->id, $post->categories->pluck('id')->toArray()) ? 'selected' : ''}}>{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Content</label>
								<textarea rows="7" class="form-control" name="content" id="content" required>{{old('content', $post->content)}}</textarea>
							</div>
							<div class="form-group">
								<label>Status</label>
								@php
								$disabled = 'required';
								if (!Auth::user()->can('permission:publish posts')) {
									if ($post->status != 1) {
										$disabled = 'disabled';
									}
								}
								@endphp
								<select class="form-control" onchange="getDate(this)" name="status" {{$disabled}}>
									@if($post->status == 2)
									<option value="2" selected {{ Auth::user()->can('permission:publish posts') ? '' : 'disabled' }}>Published</option>
									<option value="4" {{ Auth::user()->can('permission:unpublish posts') ? '' : 'disabled' }}>Unpublished</option>
									@else
									<option value="1" {{$post->status == 1 ? 'selected' : ''}}>Draft</option>
									<option value="2" {{$post->status == 2 ? 'selected' : ''}} {{ Auth::user()->can('permission:publish posts') ? '' : 'disabled' }}>Published</option>
									<option value="3" {{$post->status == 3 ? 'selected' : ''}} {{ Auth::user()->can('permission:publish posts') ? '' : 'disabled' }}>Schedule</option>
									@endif
								</select>
							</div>
							<div class="form-group {{$post->status == 3 ? '' : 'd-none'}}">
								<label>Date</label>
								@php
								if (empty($post->published_at)) {
									$published_at = date('Y-m-d H:i:s');
								}else{
									$published_at = date('Y-m-d H:i:s') > $post->published_at ? $post->published_at : date('Y-m-d H:i:s');
								}
								$published_at = strtotime($published_at);		
								@endphp
								<input type="datetime-local" class="form-control" name="published_at" min="{{date('Y-m-d\TH:i', $published_at)}}" value="{{old('published_at', (empty($post->published_at) ? null : date('Y-m-d\TH:i', strtotime($post->published_at))))}}" {{Auth::user()->can('permission:publish posts') ? '' : 'disabled'}}>
							</div>
							<div class="form-group">
								<label>Meta Title</label>
								<input type="text" class="form-control" name="meta_title" value="{{old('meta_title', $post->meta_title)}}">
							</div>
							<div class="form-group">
								<label>Meta Description</label>
								<textarea name="meta_description" rows="2" class="form-control">{{old('meta_description', $post->meta_description)}}</textarea>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" onclick="location.href='{{ url()->previous() }}'" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
							<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/select2.js')}}"></script>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
		CKEDITOR.replace('content', {
			filebrowserUploadUrl: "{{route('dashboard.post.upload', ['_token' => csrf_token() ])}}",
			filebrowserUploadMethod: 'form',
			height: '500px',
			disallowedContent: 'script; *[on*]',
			// removePlugins: 'image,pwimage',
			removeDialogTabs: 'image:info;image:Link;image:advanced',
		});

	});

	function str_slug(self){
		var title = $(self).val();
		title = title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
		$('input[name="slug"]').val(title);
	}

	function edit_slug(self)
	{
		var btn  = $(self);
		var slug = $('input[name="slug"]');
		if(btn.hasClass('btn-warning')) {
			slug.attr('readonly', false);
			btn.toggleClass('btn-warning btn-success');
			btn.html('<i class="fa fa-save"></i> Save');
			$('input[name="title"]').attr('onkeyup', '');
		} else {
			slug.attr('readonly', true);
			btn.toggleClass('btn-success btn-warning');
			btn.html('<i class="fa fa-pencil"></i> Edit');
			$('input[name="title"]').attr('onkeyup', 'str_slug(this)');
		}
	}

	function getDate(self)
	{
		var date = $('input[name="published_at"]');
		if ($(self).val() == 3) {
			date.parents('div.form-group').removeClass('d-none');
			date.attr('required', true);
		}else{
			date.parents('div.form-group').addClass('d-none');
			date.attr('required', false);
			date.val('');
		}
	}
</script>
@endsection