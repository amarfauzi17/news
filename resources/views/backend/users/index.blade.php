@extends('layouts.backend')
@section('title', 'AdminLTE 3 | User Index')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/select2.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
	.select2-container--default .select2-selection--single{
		height: 38px;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice{
		color:#000;
	}
	.list-permissions{
		-webkit-column-count: 3;
		-moz-column-count: 3;
		column-count: 3;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						@can('create users')
						<button type="button" onclick="$('#modal-create').modal('show')" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create</button>
						&nbsp;&nbsp;&nbsp;&nbsp;
						@endcan
						<h3 class="card-title float-right">DataTable Users</h3>
					</div>
					<div class="card-body">
						<table id="tbl-user" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="5%">No</th>
								<th>User</th>
								<th>Email</th>
								<th>Roles</th>
								@canany(['edit users', 'delete users', 'reset password users'])
								<th width="10%">Action</th>
								@endcan
							</thead>
							<tbody>
								@foreach($users as $user)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									@if($user->roles->count() > 0)
									<td>
										<ul style="padding-left: 15px;">
											@foreach($user->roles as $role)
											<li>
												<b>{{ucwords($role->name)}}</b>:<br> 
												@foreach($role->permissions as $permission)
												<span class="badge badge-secondary mr-2 mt-2 p-1">{{ucwords($permission->name)}}</span>
												@endforeach
											</li>
											@endforeach
										</ul>
									</td>
									@else
									<td><center><i>No Item</i></center></td>
									@endif
									@canany(['edit users', 'delete users', 'reset password users'])
									<td class="text-nowrap">
										@can('edit users')
										<button class="btn btn-xs btn-warning" data-id="{{$user->id}}" data-name="{{$user->name}}" data-email="{{$user->email}}" data-roles="{{$user->roles->pluck('name')->toJson()}}" onclick="modalEdit(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-edit"></i></button>
										&nbsp;
										@endcan
										@can('reset password users')
										<button class="btn btn-xs btn-primary" data-id="{{$user->id}}" data-name="{{$user->name}}" onclick="modalReset(this)"><i class="fa fa-key"></i></button>
										&nbsp;
										@endcan
										@can('delete users')
										<button class="btn btn-xs btn-danger" data-id="{{$user->id}}" data-name="{{$user->name}}" onclick="modalDelete(this)" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
										@endcan
									</td>
									@endcan
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@can('create users')
<div class="modal fade" id="modal-create" tabindex="-1" user="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="{{route('dashboard.user.store')}}">
				@csrf
				<div class="modal-header bg-primary">
					<h4 class="modal-title">Create user</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" minlength="6" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" minlength="6" name="repassword" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Roles</label>
						<select class="form-control select2" multiple name="roles[]" onchange="getPermission(this)" required>
							@foreach($roles as $role)
							<option value="{{$role->name}}" data-permissions="{{$role->permissions->pluck('name')->toJson()}}">{{ucwords($role->name)}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group d-none" id="form-permissions">
						<label>Permissions</label>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit users')
<div class="modal fade" id="modal-edit" tabindex="-1" user="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-warning">
					<h4 class="modal-title">Edit user</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Roles</label>
						<select class="form-control select2" multiple name="roles[]" onchange="getPermission(this)" required>
							@foreach($roles as $role)
							<option value="{{$role->name}}" data-permissions="{{$role->permissions->pluck('name')->toJson()}}">{{ucwords($role->name)}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group d-none" id="form-permissions">
						<label>Permissions</label>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-warning">Upadte</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('reset password users')
<div class="modal fade" id="modal-reset" tabindex="-1" user="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-primary">
					<h4 class="modal-title">Reset Password User</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" minlength="6" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" name="repassword" minlength="6" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-primary">Upadte</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete users')
<div class="modal fade" id="modal-delete" tabindex="-1" user="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete user</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete user <b id="modal-delete-user"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('js/backend/select2.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-user').DataTable();
		$('.select2').select2();
	});

	function getPermission(self)
	{
		var modal   = $(self).parents('.modal');
		var results = [];
		var data = $(self).find("option:selected");
		if (data.length > 0) {
			data.each(function(el){
				var roles = $(this).data('permissions');
				roles.forEach(function(role){
					results.push(role);
				});
			});
			results  = results.filter(onlyUnique);
			var html = "<ul class='list-permissions'>";
			results.forEach(function(permission){
				html += "<li>"+permission+"</li>"
			});
			html += "</ul";
			modal.find('#form-permissions').removeClass('d-none');
			modal.find('#form-permissions').empty();
			modal.find('#form-permissions').append(html);
		}else{
			modal.find('#form-permissions').addClass('d-none');
			modal.find('ul.list-permissions').remove();
		}
	}

	function onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}

	@can('edit users')
	function modalEdit(self)
	{
		var id    = $(self).attr('data-id');
		var name  = $(self).attr('data-name');
		var email = $(self).attr('data-email');
		var roles = JSON.parse($(self).attr('data-roles'));
		var modal = $('#modal-edit');
		var url   = '{{ route("dashboard.user.update", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('input[name="name"]').val(name);
		modal.find('input[name="email"]').val(email);
		modal.find('select[name="roles[]"]').val(roles).change();
		modal.modal('show');
	}
	@endcan

	@can('reset password users')
	function modalReset(self)
	{
		var id    = $(self).attr('data-id');
		var name  = $(self).attr('data-name');
		var modal = $('#modal-reset');
		var url   = '{{ route("dashboard.user.reset-password", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.modal('show');
	}
	@endcan

	@can('delete users')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.user.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-user').html('('+$(self).attr('data-name')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection