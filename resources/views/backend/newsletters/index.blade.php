@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Newsletter View')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">DataTable Newsletters</h3>
					</div>
					<div class="card-body">
						<table id="tbl-newsletter" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="5%">No</th>
								<th>Email</th>
								@can('delete newsletters')
								<th width="5%">#</th>
								@endcan
							</thead>
							<tbody>
								@foreach($newsletters as $newsletter)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$newsletter->email}}</td>
									@can('delete newsletters')
									<td>
										<button class="btn btn-sm btn-danger" onclick="modalDelete(this)" data-id="{{$newsletter->id}}" data-email="{{$newsletter->email}}" type="button"><i class="fa fa-trash"></i></button>
									</td>
									@endcan
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@can('delete newsletters')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete Newsletter</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete newsletter <b id="modal-delete-newsletter"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-newsletter').DataTable();
	});

	@can('delete newsletters')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.newsletter.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-newsletter').html('('+$(self).attr('data-email')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection