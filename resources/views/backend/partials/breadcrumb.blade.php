@php
$segments = '';
$total    = count(Request::segments());
$routes   = explode('.', str_replace("dashboard.", '', \Request::route()->getName()));
@endphp
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">
					{{ ucfirst(reset($routes)) }}
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					@foreach(Request::segments() as $segment)
					@php $segments.= '/'.$segment; @endphp
					@if($loop->iteration == $total)
					<li class="breadcrumb-item active">{{$segment}}</li>
					@else
					<li class="breadcrumb-item"><a href="{{ $segments }}">{{$segment}}</a></li>
					@endif
					@endforeach
				</ol>
			</div>
		</div>
	</div>
</div>