<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="index3.html" class="brand-link">
		<img src="{{asset('images/adminlte/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light">AdminLTE 3</span>
	</a>
	<div class="sidebar">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="info text-center">
				<a href="#" class="d-inline-block">{{Auth::user()->name}}</a>
				<span class="text-white">Login as {{implode(", ", Auth::user()->roles->pluck('name')->toArray()) ?? ''}}</span>
			</div>
		</div>
		{{-- <div class="form-inline">
			<div class="input-group" data-widget="sidebar-search">
				<input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
				<div class="input-group-append">
					<button class="btn btn-sidebar">
						<i class="fas fa-search fa-fw"></i>
					</button>
				</div>
			</div>
		</div> --}}

		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item {{set_active('dashboard.index', 'menu-open')}}">
					<a href="{{route('dashboard.index')}}" class="nav-link {{set_active('dashboard.index')}}">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>
				@canany(['view posts', 'create posts', 'edit posts', 'delete posts', 'publish posts', 'unpublish posts'])
				<li class="nav-item {{set_active(['dashboard.post.index', 'dashboard.post.create', 'dashboard.post.edit'], 'menu-open')}}">
					<a href="#" class="nav-link {{set_active(['dashboard.post.index', 'dashboard.post.create', 'dashboard.post.edit'])}}">
						<i class="nav-icon fas fa-edit"></i>
						<p>
							Posts
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{route('dashboard.post.index')}}" class="nav-link {{set_active('dashboard.post.index')}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Index</p>
							</a>
						</li>
						@can('create posts')
						<li class="nav-item">
							<a href="{{route('dashboard.post.create')}}" class="nav-link {{set_active('dashboard.post.create')}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Create</p>
							</a>
						</li>
						@endcanany
					</ul>
				</li>
				@endcan
				@canany(['view pages', 'create pages', 'edit pages', 'delete pages'])
				<li class="nav-item {{set_active(['dashboard.page.index', 'dashboard.page.create', 'dashboard.page.edit'], 'menu-open')}}">
					<a href="#" class="nav-link {{set_active(['dashboard.page.index', 'dashboard.page.create', 'dashboard.page.edit'])}}">
						<i class="nav-icon fas fa-book"></i>
						<p>
							Pages
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{route('dashboard.page.index')}}" class="nav-link {{set_active('dashboard.page.index')}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Index</p>
							</a>
						</li>
						@can('create pages')
						<li class="nav-item">
							<a href="{{route('dashboard.page.create')}}" class="nav-link {{set_active('dashboard.page.create')}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Create</p>
							</a>
						</li>
						@endcanany
					</ul>
				</li>
				@endcan
				@canany(['view categories', 'create categories', 'edit categories', 'delete categories'])
				<li class="nav-item {{set_active('dashboard.category.index', 'menu-open')}}">
					<a href="{{route('dashboard.category.index')}}" class="nav-link {{set_active('dashboard.category.index')}}">
						<i class="nav-icon fas fa-bars"></i>
						<p>
							Category
						</p>
					</a>
				</li>
				@endcan
				@canany(['view comments', 'delete comments', 'reply comments'])
				<li class="nav-item {{set_active('dashboard.comment.index', 'menu-open')}}">
					<a href="{{route('dashboard.comment.index')}}" class="nav-link {{set_active('dashboard.comment.index')}}">
						<i class="nav-icon far fa-comments"></i>
						<p>
							Comment
						</p>
					</a>
				</li>
				@endcan
				@canany(['view messages', 'delete messages'])
				<li class="nav-item {{set_active('dashboard.message.index', 'menu-open')}}">
					<a href="{{route('dashboard.message.index')}}" class="nav-link {{set_active('dashboard.message.index')}}">
						<i class="nav-icon far fa-envelope"></i>
						<p>
							Message
						</p>
					</a>
				</li>
				@endcan
				@canany(['view newsletters', 'delete newsletters'])
				<li class="nav-item {{set_active('dashboard.newsletter.index', 'menu-open')}}">
					<a href="{{route('dashboard.newsletter.index')}}" class="nav-link {{set_active('dashboard.newsletter.index')}}">
						<i class="nav-icon fas fa-newspaper"></i>
						<p>
							Newsletter
						</p>
					</a>
				</li>
				@endcan
				@role('admin')		
				<li class="nav-item {{set_active(['dashboard.role.index', 'dashboard.user.index'], 'menu-open')}}">
					<a href="#" class="nav-link {{set_active(['dashboard.role.index', 'dashboard.user.index'])}}">
						<i class="nav-icon far fa-user"></i>
						<p>
							User Management 
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{route('dashboard.role.index')}}" class="nav-link {{set_active('dashboard.role.index')}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Roles</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{route('dashboard.user.index')}}" class="nav-link {{set_active('dashboard.user.index')}}">
								<i class="far fa-circle nav-icon"></i>
								<p>users</p>
							</a>
						</li>
					</ul>
				</li>
				@endrole
				<li class="nav-item {{set_active('dashboard.setting.index', 'menu-open')}}">
					<a href="{{route('dashboard.setting.index')}}" class="nav-link {{set_active('dashboard.setting.index')}}">
						<i class="nav-icon fas fa-cog"></i>
						<p>
							Setting
						</p>
					</a>
				</li>
			</ul>
		</nav>
	</div>
</aside>