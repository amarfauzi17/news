@extends('layouts.backend')
@section('title', 'AdminLTE 3 | Message View')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/backend/datatable.css')}}">
<style type="text/css">
	#tbl-posts_paginate, #tbl-posts_filter{
		float: right;
	}
</style>
@endsection
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">DataTable Messages</h3>
					</div>
					<div class="card-body">
						<table id="tbl-message" class="table table-bordered table-hover dataTable">
							<thead>
								<th width="5%">No</th>
								<th>Name</th>
								<th>Email</th>
								<th>Website</th>
								<th>Phone</th>
								<th>Subject</th>
								<th width="35%">Message</th>
								@can('delete messages')
								<th width="5%">#</th>
								@endcan
							</thead>
							<tbody>
								@foreach($messages as $message)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$message->name}}</td>
									<td>{{$message->email}}</td>
									<td>{{$message->website}}</td>
									<td>{{$message->phone}}</td>
									<td>{{$message->subject}}</td>
									<td>{{$message->message}}</td>
									@can('delete messages')
									<td>
										<button class="btn btn-sm btn-danger" onclick="modalDelete(this)" data-id="{{$message->id}}" data-name="{{$message->name}}" type="button"><i class="fa fa-trash"></i></button>
									</td>
									@endcan
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@can('delete messages')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h4 class="modal-title">Delete Messages</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).parents('.modal').modal('hide')">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete message <b id="modal-delete-message"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/backend/datatable.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#tbl-message').DataTable();
	});

	@can('delete messages')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.message.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-message').html('('+$(self).attr('data-name')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endsection