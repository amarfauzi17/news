const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 X*/


 mix.js('resources/js/bootstrap.js', 'public/js');
 mix.js('resources/js/jquery.js', 'public/js');

 const adminlte = "node_modules/admin-lte/";

 mix.css(adminlte+"plugins/fontawesome-free/css/all.css", 'public/css/backend/');
 mix.css(adminlte+"plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css", 'public/css/backend/');
 mix.css(adminlte+"plugins/icheck-bootstrap/icheck-bootstrap.css", 'public/css/backend/');
 mix.css(adminlte+"plugins/jqvmap/jqvmap.css", 'public/css/backend/');
 mix.css(adminlte+"dist/css/adminlte.css", 'public/css/backend/');
 mix.css(adminlte+"plugins/overlayScrollbars/css/OverlayScrollbars.css", 'public/css/backend/');
 mix.css(adminlte+"plugins/daterangepicker/daterangepicker.css", 'public/css/backend/');
 // mix.css(adminlte+"plugins/summernote/summernote-bs4.css", 'public/css/backend/');
 // mix.css("node_modules/summernote/dist/summernote.css", 'public/css/backend/');

 mix.sass('resources/sass/awesome.scss', 'public/css/backend/');

 mix.js(adminlte+'plugins/jquery/jquery.js', 'public/js/backend');
 mix.js(adminlte+'plugins/jquery-ui/jquery-ui.js', 'public/js/backend');
 mix.js(adminlte+'plugins/bootstrap/js/bootstrap.bundle.js', 'public/js/backend').sourceMaps();
 mix.js(adminlte+'plugins/chart.js/Chart.js', 'public/js/backend');
 mix.js(adminlte+'plugins/sparklines/sparkline.js', 'public/js/backend');
 mix.js(adminlte+'plugins/jqvmap/jquery.vmap.js', 'public/js/backend');
 mix.js(adminlte+'plugins/jqvmap/maps/jquery.vmap.usa.js', 'public/js/backend');
 // mix.js(adminlte+'plugins/jquery-knob/jquery.knob.min.js', 'public/js/backend');
 // mix.js(adminlte+'plugins/moment/moment.min.js', 'public/js/backend');
 mix.js(adminlte+'plugins/daterangepicker/daterangepicker.js', 'public/js/backend');
 mix.js(adminlte+'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js', 'public/js/backend');
 // mix.js(adminlte+'plugins/summernote/summernote-bs4.js', 'public/js/backend');
 mix.js(adminlte+'plugins/overlayScrollbars/js/jquery.overlayScrollbars.js', 'public/js/backend');
 mix.js(adminlte+'dist/js/adminlte.js', 'public/js/backend');
 mix.js(adminlte+'dist/js/demo.js', 'public/js/backend');
 mix.js(adminlte+'dist/js/pages/dashboard.js', 'public/js/backend');
 mix.js(adminlte+'plugins/select2/js/select2.full.js', 'public/js/backend');

 mix.js("resources/js/ckeditor.js", 'public/js/backend/');
 // mix.js("node_modules/ckeditor4/ckeditor.js", 'public/js/backend/');
 // mix.js("resources/js/ckfinder.js", 'public/js/backend/');

 // mix.sass('resources/sass/awesome.scss', 'public/css/backend/');

 mix.styles([
 	adminlte+'plugins/select2/css/select2.css',
 	adminlte+'plugins/select2-bootstrap4-theme/select2-bootstrap4.css',
 	], 'public/css/backend/select2.css');

 mix.styles([
 	adminlte+'plugins/datatables-bs4/css/dataTables.bootstrap4.css',
 	adminlte+'plugins/datatables-responsive/css/responsive.bootstrap4.css',
 	adminlte+'plugins/datatables-buttons/css/buttons.bootstrap4.css',
 	], 'public/css/backend/datatable.css');

 mix.scripts([
 	adminlte+'plugins/select2/js/select2.full.js',
 	], 'public/js/backend/select2.js');

 mix.scripts([
 	adminlte+'plugins/datatables/jquery.dataTables.js',
 	adminlte+'plugins/datatables-bs4/js/dataTables.bootstrap4.js',
 	adminlte+'plugins/datatables-responsive/js/dataTables.responsive.js',
 	adminlte+'plugins/datatables-responsive/js/responsive.bootstrap4.js',
 	adminlte+'plugins/datatables-buttons/js/dataTables.buttons.js',
 	adminlte+'plugins/datatables-buttons/js/buttons.bootstrap4.js',
 	], 'public/js/backend/datatable.js');

 mix.copy(adminlte+'dist/img', 'public/images/adminlte');
